'use strict'

require('dotenv').config()
let Sequelize = require('sequelize')
const { db } = require('../utlis/db');


let Usuario = db.define('Usuario', {
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    cEmail:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cNombre:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cPassword:{
        type: Sequelize.STRING,
        allowNull: false
    }
})

db.sync()

module.exports = Usuario