'use strict'

require('dotenv').config()
let Sequelize = require('sequelize')
const { db } = require('../utlis/db');


let Message = db.define('Message', {
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    cFecha:{
        type:Sequelize.STRING,
        allowNull: false
    },
    cUid:{
        type:Sequelize.STRING,
        allowNull: false
    },
    iTipo:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    cMensaje:{
        type:Sequelize.STRING,
        allowNull: false
    }
})

db.sync()
module.exports = Message