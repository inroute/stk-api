'use strict'

require('dotenv').config()
let Sequelize = require('sequelize')
const { db } = require('../utlis/db');


let Vehiculo = db.define('Vehiculo', {
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    webfleetUid:{
        type: Sequelize.STRING,
        allowNull: false
    },
    configurado:{
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
})

db.sync()

module.exports = Vehiculo