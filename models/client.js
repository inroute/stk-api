'use strict'

require('dotenv').config()
const Sequelize = require('sequelize')
const { db } = require('../utlis/db');


let Cliente = db.define('Cliente', {
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    cNombre:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cAlias:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cContacto:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cTelContacto:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cCuenta:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cPassword:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cUsuario:{
        type: Sequelize.STRING,
        allowNull: false
    },
    cApiKey:{
        type: Sequelize.STRING,
        allowNull: false
    }
})

db.sync()

module.exports = Cliente