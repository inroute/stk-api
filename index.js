"use strict";
require('dotenv').config()
const app = require("./app");
const { db } = require('./utlis/db')
const port = process.env.PORT || 8080;


db
  .authenticate()
  .then(() => {
    console.log("Connection to DB has been established successfully.");
    app.listen(port, function() {
      console.log("app running on port: " + port);
    });
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

